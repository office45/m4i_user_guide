============================
Models4Insight Documentation
============================

Welcome to the documentation for Models4Insight!

Models4Insight is a Software as a Service solution, that helps you to manage the lifecycle of your ArchiMate models.
It consists of 4 parts: 

- Model Repository
- Platform
- Analytics Library
- Data2Model
- Consistency Metrics

By storing your models in the Models4Insight **Model Repository**, you create a single source of truth which can be used for analysis. 
The repository is accessible via the Models4Insight **Platform**, which also provides model management features, such as:

- Collaboration
- Versioning
- Conflict management
- Branching

Data2Model helps you to automatically generate ArchiMate models from structured datasets like Excel spreadsheets, CSV, or JSON. 
This includes deriving concepts from your data, as well as arranging them into meaningful views.
Data2Model integrates well with the Models4Insight repository and platform.

The analytics library provides a basis for automated model generation, as well as advanced analytics using models. It is available as a Python distribution upon request.

If you are new to Models4Insight, please have a look at our getting started guides!

Getting started guides
======================

:doc:`Get started with the Models4Insight Platform <platform>`

:doc:`Get started with Data2Model <data2model>`

:doc:`Get started with Models4Insight Consistency Metrics <consistency_metrics>`

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Getting started guides

   platform
   data2model
   consistency_metrics


