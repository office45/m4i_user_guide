User Roles
==========

The default role is the Business User. This role has viewing rights on the portal or workflow in case they are granted to the business user in the respective system.
The following systems are available:
 - Repository
 - Workflow
 - Portal 

Repository Roles
~~~~~~~~~~~~~~~~
+----------------------------+----------------------------------------------------------------------------------+
|         User Role          |                                  Description                                     |
+============================+==================================================================================+
|      Owner                 |  The owner of a project has full control over the project. A project has always  |
|                            |  an owner and only owners can remove another owners access rights.               |
+----------------------------+----------------------------------------------------------------------------------+
|     Maintainer             |  A maintainer can assign rights, publish models, and manages models.             |
+----------------------------+----------------------------------------------------------------------------------+
|   Contributer              | A Contributer can manage models and branches, but can not manage project settings. |
+----------------------------+----------------------------------------------------------------------------------+
|  Model User                | A model users can retrieve and upload models into the repository                 |
+----------------------------+----------------------------------------------------------------------------------+
|       Business User        | Does not have any rights on the Repository										|
+----------------------------+----------------------------------------------------------------------------------+

Workflow Roles
~~~~~~~~~~~~~~
All roles have access to the workflow system, however they will not see any information initially.
Access for individual users is managed in the workflow system itself. 
The rights are transitive for nested process groups, thus manage your workflows in such a way that you have one process group  


Portal Roles
~~~~~~~~~~~~
All roles have access to the portal, but maybe not to the dashboards in the portal.
Access is managed in the portal by the owner or maintainer role. In the portal access can be granted per data source, 
while a user can only see the dashboards where access to the used data sources has been granted.

When a project in the platform is published to the portal, all rights in the portal are set accordingly for the associated users of the project.
If the project settings are changed later on and users or their roles are changed these changes are reflected in the portal roles automatically. 