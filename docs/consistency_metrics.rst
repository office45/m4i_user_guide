=============================================
Models4Insight Consistency Metrics User Guide
=============================================

.. toctree::
    :maxdepth: 2
    :numbered:
    
About Consistency Metrics
=========================

`Models4Insight Consistency Metrics <https://www.models4insight.com/consistency_metrics>`_ is an analytics software as a service solution to assess the quality of an ArchiMate model by automatically generating a quality assessment report called **consistency metric report**.
Quality of an enterprise architecture model in the context of this service is based on a set of architecture modeling guidelines and reports the compliance of the actual enterprise architecture model (EA model) with these guidelines. 

Please note that the consistency of an EA model does not reflect whether or not the enterprise architecture model completely represents the modeled environment.
Rather, it reflects whether the EA model that has been constructed adheres to agreed upon best practices structurally.

This guide is intended to help you get started with Models4Insight Consistency Metrics quickly. 
It describes the basic steps from generating an architecture model quality report, to taking action to improve your architecture model. 
To be able to apply a consistency check to your EA model, you must have a model uploaded in the Models4Insight Platform. 

Key concepts
------------

|metric_context|

The basis of the quality report are the best-practice architecture modeling guidelines.
You can read more about these architecture modeling guidelines here **<ADD A LINK TO THE GUIDELINES DOCS>**. 

The Models4Insight Consistency Metrics application allows you to easily check the degree to which your enterprise architecture model adheres to these guidelines.
It generates a report which lists all cases where your model does not conform with the guidelines in some way.
Such a case is called a **consistency metric non-compliance condition**.
Please note that some of these conditions are applicable to multiple architecture modeling guidelines and therefore are described as **consistency metrics** related to an architecture modeling guideline.

For every non-compliance condition, the report contains detailed information about the **non-compliant EA model concepts** as well as a summary of the checks performed. 
This summary contains information about the amount of compliant and non-compliant concepts, as well as the number of exemptions.

An **exemption** can be made for a non-compliance condition, of which an architect has determined that it is an intended violation of a best-practice EA modelling guideline. 
Exemptions are made on an individual concept basis, and apply only to a single consistency metric.
Thus, while the EA model may still deviate a particular modelling guideline, the non-compliance conditions for which exemptions have been made will not show up in the report.

Models4Insight Consistency Metrics integrates with the Models4Insight platform and repository for version management and model persistence. 
Every version of an EA model stored in the repository can be assessed using the consistency metrics service.

Registration
============

If you are not already a Models4Insight user, you will need to register first. To do so, please visit
`www.models4insight.com <https://www.models4insight.com>`_ and click
the **register** button.

Clicking the register button takes you to the user registration page
where you are asked to provide some personal information. **Note: A
username cannot use capital letters, special characters or double
spaces.**

Once you have filled in all the information, click on register. You will
receive a verification email at the address you provided. You need to
verify your registration before you can log in.

You should immediately receive an email from Aurelius Enterprise. The
link within is valid for 5 minutes. Check your spam folder if you
cannot find the email. If 5 minutes have passed, please click the
re-send email button.

Login
=====

All Models4Insight services use a single sign-on. This means
you only need to log in once to use all services. If you are not already logged in, you will be prompted to do so when you initially visit Models4Insight Consistency Metrics.

Installation
============

If you are a mobile user, or if you are using Chrome, you have the option to install Models4Insight Consistency Metrics as an app on your device. 
This will add a shortcut to your home screen or desktop, and will allow Models4Insight Consistency Metrics to run as a dedicated app/window. 
You will find the installation button in the navigation menu.

It is recommended that you install the app if you intend to use Models4Insight Consistency Metrics regularly. You will not miss out on any functionality if you do not install or if you are using a different browser.

Workflow
========

|workflow|

The basic workflow for Models4Insight Consistency Metrics is as follows:

1. Choose an enterprise architecture model for which you want to make a quality report
2. Generate for the selected enterprise architecture model a consistency metric report
3. Review the consistency metrics non-compliances
4. Make changes to improve your enterprise architecture model based on the results of the consistency metrics analysis
5. Commit your improved enterprise architecture model to the Models4Insight repository via the Models4Insight Platform
6. Run the analysis again to see how the quality of the enterprise architecture model has improved

The remainder of the guide below will take you through each of the above steps in detail.

Choose an enterprise architecture model
---------------------------------------

|generate_report|

The first step in generating an consistency metric report is to choose the enterprise architecture model on which the report will be based. The model must be selected from the Models4Insight repository, thus three pieces of information are needed to specify the enterprise architecture model:

- To which project the enterprise architecture model belongs
- To which branch the enterprise architecture model belongs
- For which version of the enterprise architecture model you want to generate a report

When you first open the Models4Insight Consistency Metrics application, you will see the 'Generate a report' page. 

On this page, you are first asked to select the project to which your enterprise architecture model belongs.
The select box shows the names of the projects to which you have access.
You can see more details by clicking the search button next to the select box.

After choosing a project, the application will present the branches which belong to this project.
Please select the branch to which your enterprise architecture model belongs.
Again, if you need more information about the branches, click the search button next to the select box.

After choosing a branch, the versions of the enterprise architecture model which belong to this branch will be presented.
Finally, you need to select the version of the enterprise architecture model for which you want to generate the consistency metrics report.
The list will be sorted in decreasing order of the upload date of the enterprise architecture model.
Details for each enterprise architecture model version are available by clicking the search button.

Generate the report
-------------------

After choosing an enterprise architecture model version, the 'Generate report' button at the bottom of the page will light up.
When you click this button, you will be redirected to the consistency metrics report page for the enterprise architecture model you have selected.

You can share the URL for the consistency metrics report you have generated with anyone who has access to the same enterprise architecture model version.

Review the consistency metrics report
-------------------------------------

|report_overview|

Once you have selected an enterprise architecture model and generated a report, you can now browse through the related consistency metric report to learn about the consistency of your enterprise architecture model.

On the left side of the page is a table of contents which shows all available consistency metrics and their grouping into different consistency metric categories. These metrics are:

============================= ========== ====================================================================================================================================================================
Metric                        Category   Description
============================= ========== ====================================================================================================================================================================
Actor Role Assignment         Pattern    These relationships are not assignment between business actors, business roles and business processes, thus non compliant
Copy Text                     Textual    These elements were copied but still include "(copy)" in their label
Cycles                        Pattern    The set of elements connected via aggregation or composition relationships that form a cycle
Equipment Facility Assignment Pattern    These relationships are not assignment between equipment and facilities, thus non compliant
Facility Structure            Pattern    These relationships are not aggregation/composition/realization between facilities and facilities, thus non compliant
Process Event Flow            Pattern    These relationships are not triggering between events and processes of the same type, or events and junctions, thus non compliant
Process Junctions             Pattern    These relationships are not triggering/aggregation/composition between similar-type processes or triggering/flow between processes and junctions, thus non compliant
Process Trigger Flow          Pattern    These elements connect to multiple in- or outgoing, flow or trigger relationships
Sentence                      Textual    The names of these concepts are not structured as a sentence
Single Trigger Flow           Pattern    These process steps trigger other process steps at different abstraction levels in the model
Tree Property                 Pattern    The child elements connect via aggregation or composition to more than one parent, and thus violates the tree property
Trigger Context               Structural These process steps trigger other process steps at different abstraction levels in the model
Unconnected Elements          Structural These elements are not connected to any other elements
View Based                    Structural These elements are not used in any ArchiMate Views
============================= ========== ====================================================================================================================================================================

By clicking on each of the consistency metric categories (the blue headers), you will get an overview of all underlying consistency metrics and a summary of their results.
By clicking on a specific consistency metric in the table of contents, you will see the detailed results for that metric specifically.

|report_metric|

When you first open a consistency metric, you will see a table listing all concepts in the model which do not comply with the consistency metric conditions.
For every concept, the table will show what has been determined to be the non-compliance.
Now you have three options:

1. You try to solve the non-compliance by modifying the enterprise architecture model
2. You create an exemption for this specific non-compliance, thus, this non-compliance will not be shown in a new version of the consistency metrics report
3. You ignore the non-compliance without creating an exemption, thus, this non-compliance will be presented in a new version of the consistency report

The section 'Improve your model' covers how to solve a non-compliance by modifying the enterprise architecture model. In the following the management of exemptions is discussed.

Exemptions
~~~~~~~~~~

There may, however, be reasons why you do not want to change your enterprise architecture model. 
For example, the consistency metrics check marks the name **MES**, which stands for Manufacturing Execution System, not compliant for concept labels. However, the abbreviation MES is a technical term, which all users of the model recognize and would write the same way.
In this case, you make a conscious decision to deviate for this concept name from the architecture modeling guidelines and therefore add an exemption for the MES concept.
As a consequence, the MES concept will no longer be recognized as a non-compliance for the naming of concepts metrics.

You add an exemption by clicking the 'add exemption' button.
A window will open, asking you to provide some details on the exemption.

|exemption_modal|

You need to choose whether to exempt this case from the check for this version of the enterprise architecture model, for the branch the project, or for the entire project:

=============== ==================================================
Exemption Scope Description
=============== ==================================================
This project    This exemption should apply to the whole project
This branch     This exemption should apply to the current branch
This version    This exemption should apply to the current version
=============== ==================================================

Once you have selected a scope, you need to add a short description of why you have made the exemption.
Afterwards, click 'Save' to save the exemption, which will also close the window.

|exemptions_tab|

You can see the exemption that you have created, along with any other ones that apply to the current metric, by looking at the 'Exemptions' tab.
Here, you can edit the exemptions you have created, or delete them.


Improve your enterprise architecture model
------------------------------------------

If you have found a non-compliance which you want to resolve, the next step is to change the enterprise architecture model accordingly. 
If you have not yet downloaded the model, you can do so now by hovering over the three dots on the top of the consistency metrics report page and clicking 'Retrieve the model'.

Next, open your enterprise architecture model in Archi. 

Finding the right concept in the model
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|archi_search|

Once you have opened the enterprise architecture model, you need to find the concept in the model to which the non-compliance belongs.
The report shows an ID for every non-compliance.
This ID corresponds with a concept in the enterprise architecture model. 

To find the right concept in the enterprise architecture model using the concept ID, you can use the Archi search function.
First, copy the concept ID from the report to your clipboard.
You can do so by clicking the copy button in the corresponding row.

Next, open the model browser in Archi. This is typically the left-most section of the window. 
It shows the folder structure of your model, along with all the concept and views in it.
At the top of this section is a search button that has a looking glass icon.
Click this button to open the search bar.

To the left of the search bar is a button that allows you to specify what you are searching for.
You need to tell Archi that you are looking for a concept ID.
To do so, make sure the  'properties -> propidIEAIdentifier' setting is enabled.
An enabled setting has a check-mark next to it.

Once you have enabled the correct search option, you can paste the concept ID from your clipboard to the search bar.
Now Archi will show the concept in the model that corresponds with the given ID.
This can be an element, a relationship, or a view.

Fixing the issue
~~~~~~~~~~~~~~~~

Once you have found the right concept, you can now make changes to your enterprise architecture model to fix the non-compliance.

|archi_details|

To begin, select the concept in the Archi model browser to bring up the 'Properties' section on the bottom of the window.
This section allows you to already change basic properties such as the name of the concept, and also allows you to find related elements ,relationships and views.

To find out which elements, relationships and views are related to the concept, open at the analysis tab. 

This tab has a 'Used in views' section which lists all related views.
Double click the view name in the table to open it.

For enterprise architecture model elements, the analysis tab also has a 'Model relations' section which allows you to drill down on the connected relationships.

The required changes dependent on the consistency metrics reporting the non-compliance. Some suggestions are provided in the detailed documentation of the individual metrics.


Fixing the next issue
~~~~~~~~~~~~~~~~~~~~~

Once you are done fixing a non-compliance, you can move on to fixing the next non-compliance, following the same steps as described above. 
Repeat this until you have no more non-compliances left, or you are otherwise satisfied with the non-compliances you have fixed.

Commit your changes
-------------------

Once you are done making changes to your enterprise architecture model, you can commit it to the Models4Insight repository via the Models4Insight Platform. 
Open the project to which your enterprise architecture model belongs.
You can open your project easily by hovering over the tree dots at the top of the report page, and clicking 'Open the project'.

Next, commit your model via the 'Upload a model' tab.
If you are committing to an existing branch, you will be asked to resolve conflicts with the previous version.
By choosing the 'upload only' conflict resolution template, you apply all the changes you have made to solve the non-compliance issues.

Create a new consistency metrics report
---------------------------------------

To see the effect of the changes you have made, you can generate another consistency metrics report on the new version of the enterprise architecture model you have committed.
It is possible that your changes had unintended ripple effects in the enterprise architecture model.
The consistency metrics report will help you figure these out.

To generate the new report, follow the steps as described above, but now select the new version of your enterprise architecture model on the 'Generate a report' page.

.. |workflow| image:: media/consistency_metrics/workflow.png
   :width: 6.26806in
   :height: 3.70069in
.. |generate_report| image:: media/consistency_metrics/generate_report.png
   :width: 6.26806in
   :height: 3.70069in
.. |report_overview| image:: media/consistency_metrics/report_overview.png
   :width: 6.26806in
   :height: 3.70069in
.. |report_metric| image:: media/consistency_metrics/report_metric.png
   :width: 6.26806in
   :height: 3.70069in
.. |exemption_modal| image:: media/consistency_metrics/exemption_modal.png
   :width: 6.26806in
   :height: 3.70069in
.. |exemptions_tab| image:: media/consistency_metrics/exemptions_tab.png
   :width: 6.26806in
   :height: 3.70069in
.. |archi_search| image:: media/consistency_metrics/archi_search.png
   :width: 6.26806in
   :height: 3.70069in
.. |archi_details| image:: media/consistency_metrics/archi_details.png
   :width: 6.26806in
   :height: 3.70069in
.. |metric_context| image:: media/consistency_metrics/metric_context.png
   :width: 6.26806in
   